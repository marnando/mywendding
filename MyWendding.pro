#-------------------------------------------------
#
# Project created by QtCreator 2015-04-24T21:56:55
#
#-------------------------------------------------

QT       += core gui sql printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MyWendding
TEMPLATE = app
QMAKE_CXXFLAGS += -std=c++0x

SOURCES += src/main.cpp \
    src/view/mainwindow.cpp \
    src/controller.cpp \
    dao/connection.cpp \
    src/view/login.cpp \
    src/view/logged.cpp \
    src/view/cadastronoivos.cpp \
    src/view/contacts.cpp \
    src/view/financial.cpp \
    src/view/mainlist.cpp \
    src/view/notes.cpp \
    src/utils.cpp \
    src/view/listwidrow.cpp \
    src/view/dcalendar.cpp \
    src/control/ccadastronoivos.cpp \
    src/utils/error.cpp \
    src/view/qcustomplot.cpp \
    src/model/ncadastronoivos.cpp

HEADERS  += src/view/mainwindow.h \
    src/controller.h \
    dao/connection.h \
    src/view/login.h \
    src/view/logged.h \
    src/view/cadastronoivos.h \
    src/view/contacts.h \
    src/view/financial.h \
    src/view/mainlist.h \
    src/view/notes.h \
    src/utils.h \
    src/view/listwidrow.h \
    src/view/dcalendar.h \
    src/control/ccadastronoivos.h \
    src/utils/error.h \
    src/view/qcustomplot.h \
    src/model/ncadastronoivos.h

FORMS    += rsc/ui/mainwindow.ui \
    rsc/ui/login.ui \
    rsc/ui/logged.ui \
    rsc/ui/cadastronoivos.ui \
    rsc/ui/mainlist.ui \
    rsc/ui/contacts.ui \
    rsc/ui/financial.ui \
    rsc/ui/notes.ui \
    rsc/ui/listwidrow.ui \
    rsc/ui/dcalendar.ui \
    rsc/ui/plotchart.ui

RESOURCES += \
    rsc/qrc.qrc

DISTFILES += \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/AndroidManifest.xml \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew \
    android/gradlew.bat

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
