#ifndef CADASTRONOIVOS_H
#define CADASTRONOIVOS_H

#include <QWidget>

namespace Ui {
class CadastroNoivos;
}

class DCalendar;
class CadastroNoivos : public QWidget
{
    Q_OBJECT

private:
    DCalendar * m_calendar;

public:
    explicit CadastroNoivos(QWidget *parent = 0);
    ~CadastroNoivos();

    void initUi();

protected:
    void clearFields();
    bool check();

signals:
    void initApplication();
    void backToLogin();
    void fillCbxGender();
    void addNoivos(int a_gender, QString a_noiva, QString a_noivo, quint32 a_dataCasamento, QString a_cidade, QString a_estado);

public slots:

    void onFillCbxGender(QStringList a_list);

private slots:
    void on_m_btnCalendar_clicked();
    void on_m_btnNo_clicked();
    void on_m_btnYes_clicked();
    void on_m_cbxGenero_currentIndexChanged(int index);

private:
    Ui::CadastroNoivos * ui;
};

#endif // CADASTRONOIVOS_H
