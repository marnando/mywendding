#ifndef MAINLIST_H
#define MAINLIST_H

#include <QWidget>

namespace Ui {
    class MainList;
}

class MainList : public QWidget
{
    Q_OBJECT

private:
public:
    explicit MainList(QWidget *parent = 0);
    ~MainList();

    void appendRow(QString a_rowText);

private:
    void exampleInsertWidget(); //Only documentation about insert widget
    void addWidget(QWidget *a_widget);

signals:

public slots:

private:
    Ui::MainList * ui;

};

#endif // MAINLIST_H
