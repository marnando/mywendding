#include "ui_dcalendar.h"
#include "dcalendar.h"

DCalendar::DCalendar(QWidget *parent) : QWidget(parent),
    ui(new Ui::DCalendar)
{
    ui->setupUi(this);
}

DCalendar::~DCalendar()
{
    delete ui;
}

void DCalendar::resizeEvent(QResizeEvent *a_event)
{
    QWidget::resizeEvent(a_event);
}

void DCalendar::closeEvent(QCloseEvent *a_event)
{
    qApp->setActiveWindow(parentWidget());
    QWidget::closeEvent(a_event);
}

void DCalendar::on_m_btnCancelar_clicked()
{
    close();
    qApp->setActiveWindow(parentWidget());
}

void DCalendar::on_m_btnOk_clicked()
{
    on_m_btnCancelar_clicked();
}

void DCalendar::on_m_dEditDate_dateChanged(const QDate & a_date)
{

}

void DCalendar::on_m_calendar_clicked(const QDate & a_date)
{

}
