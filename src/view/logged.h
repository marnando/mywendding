#ifndef LOGGED_H
#define LOGGED_H

#include <QWidget>

namespace Ui {
class Logged;
}

class Logged : public QWidget
{
    Q_OBJECT
public:
    explicit Logged(QWidget *parent = 0);
    ~Logged();

signals:

public slots:

private:
    Ui::Logged * ui;
};

#endif // LOGGED_H
