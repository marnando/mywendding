#include "ui_listwidrow.h"
#include "listwidrow.h"

ListWidRow::ListWidRow(QWidget *parent) : QWidget(parent),
    ui(new Ui::ListWidRow)
{
    ui->setupUi(this);
}

ListWidRow::~ListWidRow()
{
    delete ui;
}

void ListWidRow::setWidget(QWidget *a_widget)
{
    if (a_widget != nullptr)
    {
        ui->verticalLayout->addWidget(a_widget);
    }
}

void ListWidRow::setRowText(QString a_rowText)
{
    ui->m_lblRowText->setText(a_rowText);
}
