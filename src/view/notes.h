#ifndef NOTES_H
#define NOTES_H

#include <QWidget>

namespace Ui {
    class Notes;
}

class Notes : public QWidget
{
    Q_OBJECT
public:
    explicit Notes(QWidget *parent = 0);
    ~Notes();

signals:

public slots:

private:
    Ui::Notes * ui;

};

#endif // NOTES_H
