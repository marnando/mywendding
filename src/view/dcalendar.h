#ifndef DCALENDAR_H
#define DCALENDAR_H

#include <QWidget>
#include <QCloseEvent>

namespace Ui {
    class DCalendar;
}

class DCalendar : public QWidget
{
    Q_OBJECT
public:
    explicit DCalendar(QWidget *parent = 0);
    ~DCalendar();

    void resizeEvent(QResizeEvent * a_event);
    void closeEvent(QCloseEvent * a_event);

signals:

public slots:

private slots:

    void on_m_btnCancelar_clicked();
    void on_m_btnOk_clicked();
    void on_m_dEditDate_dateChanged(const QDate &a_date);
    void on_m_calendar_clicked(const QDate &a_date);

private:
    Ui::DCalendar * ui;

};

#endif // DCALENDAR_H
