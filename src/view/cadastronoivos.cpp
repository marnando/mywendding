#include "ui_cadastronoivos.h"
#include "cadastronoivos.h"
#include "dcalendar.h"
#include "../utils.h"
#include "../utils/error.h"

#include <QDebug>
#include <QApplication>
#include <QDesktopWidget>
#include <QMessageBox>

CadastroNoivos::CadastroNoivos(QWidget *parent) : QWidget(parent),
    m_calendar(nullptr),
    ui (new Ui::CadastroNoivos)
{
    ui->setupUi(this);
    initUi();
}

CadastroNoivos::~CadastroNoivos()
{
    delete ui;
}

void CadastroNoivos::initUi()
{
    ui->m_btnCalendar->setVisible(false);
    ui->m_cbxGenero->addItem(tr("Defina o gênero:"));
    ui->m_cbxGenero->addItem(tr("M x H"));
    ui->m_cbxGenero->addItem(tr("H x H"));
    ui->m_cbxGenero->addItem(tr("M x M"));
    ui->m_cbxGenero->addItem(tr("Outros"));

    emit fillCbxGender();

//    ui->m_ledataCasamento->setInputMask("dd/mm/yyyy");
//    ui->m_ledataCasamento->setText("00000000");
//    ui->m_ledataCasamento->setValidator(new QIntValidator(1, 99999, this));
}

void CadastroNoivos::clearFields()
{
    ui->m_cbxGenero->setCurrentIndex(0);
    ui->m_leNoiva->setText("");
    ui->m_leNoivo->setText("");
    ui->m_ledataCasamento->setText("");
    ui->m_leCidade->setText("");
    ui->m_leEstado->setText("");
}

//Implementar classe de erros
bool CadastroNoivos::check()
{
    QString errorText = QString();
    bool ok = true;

    if (ui->m_cbxGenero->currentIndex() == 0)
    {
        errorText = Error::getGroomsFormError(Error::cad_noivos::ER_N_SELECT_GENDER);
    } else
    if (ui->m_leNoiva->text().isEmpty()
    ||  ui->m_leNoivo->text().isEmpty())
    {
        errorText = Error::getGroomsFormError(Error::cad_noivos::ER_FIELD_GROOMS);
    } else
    if (ui->m_ledataCasamento->text().isEmpty())
    {
        errorText = Error::getGroomsFormError(Error::cad_noivos::ER_WEDDING_DATE);
    }else
    if (ui->m_leCidade->text().isEmpty())
    {
        errorText = Error::getGroomsFormError(Error::cad_noivos::ER_CITY);
    } else
    if (ui->m_leEstado->text().isEmpty())
    {
        errorText = Error::getGroomsFormError(Error::cad_noivos::ER_STATE);
    }

    if (!errorText.isEmpty())
    {
        ok = false;
        QMessageBox::information(this, tr("Atenção"), errorText, QMessageBox::Ok);
    }

    return ok;
}

void CadastroNoivos::onFillCbxGender(QStringList a_list)
{
    foreach (QString item, a_list) {
        ui->m_cbxGenero->addItem(item);
    }
}

void CadastroNoivos::on_m_btnCalendar_clicked()
{
    if (m_calendar != nullptr)
    {
        delete m_calendar;
        m_calendar = nullptr;
    }

    m_calendar = new DCalendar(this);
    Utils::instace()->setWidgetModal(m_calendar);

    m_calendar->show();
    m_calendar->raise();
    qApp->setActiveWindow(m_calendar);
    m_calendar->setFocus();
}

void CadastroNoivos::on_m_btnNo_clicked()
{
    emit backToLogin();
}

void CadastroNoivos::on_m_btnYes_clicked()
{
    if (check())
    {
        qDebug() << "Emit add noivos";
        emit addNoivos(ui->m_cbxGenero->currentIndex()
                     , ui->m_leNoiva->text()
                     , ui->m_leNoivo->text()
                     , 12345678900
                     , ui->m_leCidade->text()
                     , ui->m_leEstado->text());

        if (!Utils::instace()->getInitApp())
        {
            qDebug() << "Inicia main window";
            emit initApplication();
        }


    }
}

void CadastroNoivos::on_m_cbxGenero_currentIndexChanged(int index)
{
    if (index > 0)
    {

    }
}
