#ifndef CONTACTS_H
#define CONTACTS_H

#include <QWidget>

namespace Ui {
class Contacts;
}

class Contacts : public QWidget
{
    Q_OBJECT
public:
    explicit Contacts(QWidget *parent = 0);
    ~Contacts();

signals:
    void showFinancial();
    void showNotes();

public slots:

private slots:
    void onCloseFinancial();

    void on_m_btnFinancial_clicked();
    void on_m_btnNotes_clicked();

private:
    Ui::Contacts * ui;

};

#endif // CONTACTS_H
