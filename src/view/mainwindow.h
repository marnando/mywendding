#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainList;
class Contacts;
class CadastroNoivos;
class Financial;
class Notes;
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    const QString MAIN_LIST     = "MainList";
    const QString CONTACTS      = "Contacts";
    const QString CAD_NOIVOS    = "CadastroNoivos";
    const QString FINANCIAL     = "Financial";
    const QString NOTES         = "Notes";

private:
    MainList *      m_mainList;
    Contacts *      m_contacts;
    CadastroNoivos * m_cadastroNoivos;
    Financial *     m_financial;
    Notes *         m_notes;

    QList<QWidget *> m_listWidgets;

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void initUI();
    void getConnects();
    void showWidget(QString a_widObjectName);
    void showMenubar(QString a_widObjectName = "");
    QString getObjectNameFromVisibleWidget();

    void closeEvent(QCloseEvent *a_event);

private slots:
    void onShowFinancial();
    void onShowNotes();

    void on_m_btnBack_clicked();
    void on_m_btnMainList_clicked();
    void on_m_btnAdd_clicked();
    void on_m_btnSettings_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
