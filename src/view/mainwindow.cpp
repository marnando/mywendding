#include "ui_mainwindow.h"
#include "mainwindow.h"
#include "mainlist.h"
#include "contacts.h"
#include "cadastronoivos.h"
#include "financial.h"
#include "notes.h"
#include <QMessageBox>
#include <QListWidget>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    m_mainList(new MainList(this)),
    m_contacts(new Contacts(this)),
    m_cadastroNoivos(new CadastroNoivos(this)),
    m_financial(new Financial(this)),
    m_notes(new Notes(this)),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    initUI();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::initUI()
{
    setWindowTitle(tr("Meu Casamento"));

    ui->m_btnBack->hide();
    ui->m_btnMainList->hide();

    m_contacts->hide();
    m_cadastroNoivos->hide();
    m_financial->hide();
    m_notes->hide();

    m_listWidgets << m_mainList
                  << m_contacts
                  << m_cadastroNoivos
                  << m_financial
                  << m_notes;

    ui->centralWidget->layout()->addWidget(m_mainList);
    ui->m_lblTitle->setText(tr("Lista de Fornecedores"));

    getConnects();
}

void MainWindow::getConnects()
{
    connect(m_contacts, SIGNAL(showFinancial()), SLOT(onShowFinancial()));
    connect(m_contacts, SIGNAL(showNotes()), SLOT(onShowNotes()));
}

void MainWindow::showWidget(QString a_widObjectName)
{
    foreach (QWidget * wid, m_listWidgets) {
        if (wid->objectName() == a_widObjectName)
        {
            showMenubar(a_widObjectName);

            ui->centralWidget->layout()->addWidget(wid);
            wid->show();
        } else
        {
            ui->centralWidget->layout()->removeWidget(wid);
            wid->hide();
        }
    }
}

void MainWindow::showMenubar(QString a_widObjectName)
{
    if (a_widObjectName == MAIN_LIST)
    {
        ui->m_btnBack->hide();
        ui->m_btnMainList->hide();
        ui->m_btnAdd->show();
    } else
    if (a_widObjectName == CONTACTS)
    {
        ui->m_btnBack->show();
        ui->m_btnMainList->show();
        ui->m_btnAdd->hide();
    } else
    if (a_widObjectName == CAD_NOIVOS)
    {
        ui->m_btnBack->show();
        ui->m_btnMainList->show();
        ui->m_btnAdd->show();
    } else
    if (a_widObjectName == FINANCIAL)
    {
        ui->m_btnBack->show();
        ui->m_btnMainList->show();
        ui->m_btnAdd->show();
    }
}

QString MainWindow::getObjectNameFromVisibleWidget()
{
    foreach (QWidget * wid, m_listWidgets) {
        if (wid->isVisible())
        {
            return wid->objectName();
        }
    }
}

void MainWindow::closeEvent(QCloseEvent *a_event)
{
    a_event->accept();
    if (getObjectNameFromVisibleWidget() == MAIN_LIST)
    {
        a_event->accept();
        QWidget::closeEvent(a_event);
    } else
    if(getObjectNameFromVisibleWidget() == FINANCIAL
    || getObjectNameFromVisibleWidget() == NOTES)
    {
        //Volto para adição de contatos
        a_event->ignore();
        on_m_btnAdd_clicked();
    } else
    {
        a_event->ignore();
        on_m_btnMainList_clicked();
    }
}

void MainWindow::onShowFinancial()
{
    showWidget(FINANCIAL);
    ui->m_lblTitle->setText(tr("Financeiro"));
}

void MainWindow::onShowNotes()
{
    showWidget(NOTES);
    ui->m_lblTitle->setText(tr("Notas"));
}

void MainWindow::on_m_btnBack_clicked()
{
    QString visibleWidObjectName = getObjectNameFromVisibleWidget();

    if(visibleWidObjectName == FINANCIAL
    || visibleWidObjectName == NOTES)
    {
        //Volto para adição de contatos
        on_m_btnAdd_clicked();
    } else
    {
        on_m_btnMainList_clicked();
    }
}

void MainWindow::on_m_btnMainList_clicked()
{
    showWidget(MAIN_LIST);
    ui->m_lblTitle->setText(tr("Lista de Fornecedores"));
}

void MainWindow::on_m_btnAdd_clicked()
{
    showWidget(CONTACTS);
    ui->m_lblTitle->setText(tr("Contato"));
}

void MainWindow::on_m_btnSettings_clicked()
{
    showWidget(CAD_NOIVOS);
    ui->m_lblTitle->setText(tr("Cadastro dos noivos"));
}
