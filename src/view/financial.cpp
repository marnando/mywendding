#include "financial.h"
#include "ui_financial.h"
#include <QMessageBox>

Financial::Financial(QWidget *parent) : QWidget(parent),
    ui (new Ui::Financial)
{
    ui->setupUi(this);
}

Financial::~Financial()
{
    delete ui;
}

void Financial::closeEvent(QCloseEvent * a_event)
{
    a_event->accept();
    QMessageBox::information(this, "Atenção", "Close Event Financeiro", QMessageBox::Ok);

    emit closeFinancial();

     QWidget::closeEvent(a_event);
}

