#include "login.h"
#include "ui_login.h"

#include <QFile>

Login::Login(QWidget *parent) : QWidget(parent),
    ui(new Ui::Login)
{
    ui->setupUi(this);
}

Login::~Login()
{
    delete ui;
}

void Login::on_m_btnCadastrar_clicked()
{
    emit initCadastroNoivos();
}
