#ifndef FINANCIAL_H
#define FINANCIAL_H

#include <QWidget>
#include <QCloseEvent>

namespace Ui {
class Financial;
}

class Financial : public QWidget
{
    Q_OBJECT
public:
    explicit Financial(QWidget *parent = 0);
    ~Financial();

    void closeEvent(QCloseEvent *a_event);

signals:
    void closeFinancial();

public slots:

private:
    Ui::Financial * ui;
};

#endif // FINANCIAL_H
