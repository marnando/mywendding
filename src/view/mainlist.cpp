#include "mainlist.h"
#include "ui_mainlist.h"
#include "listwidrow.h"
#include <QLabel>
#include <QCheckBox>
#include <QRadioButton>

MainList::MainList(QWidget *parent)
        : QWidget(parent),
          ui (new Ui::MainList)
{
    ui->setupUi(this);

    //exemplo de como deverá vir do banco
    appendRow("<b><h3><font color='green'>Teste html/ css</font></h3></b><p>"
              "<font size='3'/* color='blue'*/>This is some text, testing the real width of the label!</font>");
}

MainList::~MainList()
{
    delete ui;
}

void MainList::exampleInsertWidget()
{
    int cont = 1;

    for (int i = 0; i < 10; ++i)
    {
        // ui->m_listContacts->addItem(QString("Linha comum %1").arg(i + cont));

        cont++;
        QWidget * wid = new QWidget(this);
        QVBoxLayout * vlay = new QVBoxLayout();
        wid->setLayout(vlay);

        vlay->addWidget(new QLabel(QString("<b>%1 - Teste html</b>").arg(i/* + cont*/)));
        vlay->addWidget(new QLabel("<font size='3' color='red'>This is some text, testando o tamanho do label aqui!</font>"));
        vlay->addWidget(new QLabel("<font face='verdana' color='blue'>This is some text!</font>"));

        vlay->setSizeConstraint(QLayout::SetFixedSize);

        addWidget(wid);
    }
}

void MainList::addWidget(QWidget *a_widget)
{
    if (a_widget != nullptr)
    {

        ListWidRow * widRow = new ListWidRow(this);
        widRow->setWidget(a_widget);

        QListWidgetItem* listWidItem = new QListWidgetItem;
        listWidItem->setSizeHint( widRow->sizeHint() ); // Redimenciona de acordo com a widget
        ui->m_listContacts->addItem(listWidItem);
        ui->m_listContacts->setItemWidget(listWidItem, widRow);
    }
}

void MainList::appendRow(QString a_rowText)
{
    ListWidRow * widRow = new ListWidRow(this);
    widRow->setRowText(a_rowText);

    QListWidgetItem* listWidItem = new QListWidgetItem;
    listWidItem->setSizeHint( widRow->sizeHint() ); // Redimenciona de acordo com a widget
    ui->m_listContacts->addItem(listWidItem);
    ui->m_listContacts->setItemWidget(listWidItem, widRow);
}

