#ifndef LOGIN_H
#define LOGIN_H

#include <QWidget>

namespace Ui {
class Login;
}

class Login : public QWidget
{
    Q_OBJECT

public:
    explicit Login(QWidget *parent = 0);
    ~Login();

signals:
    void initCadastroNoivos();

public slots:

private slots:
    void on_m_btnCadastrar_clicked();

private:
    Ui::Login * ui;
};

#endif // LOGIN_H
