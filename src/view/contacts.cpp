#include "ui_contacts.h"
#include "contacts.h"
#include "financial.h"
#include "notes.h"

Contacts::Contacts(QWidget *parent) : QWidget(parent),
    ui(new Ui::Contacts)
{
    ui->setupUi(this);
}

Contacts::~Contacts()
{
    delete ui;
}

void Contacts::onCloseFinancial()
{
    this->show();
    setFocus();
}

void Contacts::on_m_btnFinancial_clicked()
{
    emit showFinancial();
}

void Contacts::on_m_btnNotes_clicked()
{
    emit showNotes();
}
