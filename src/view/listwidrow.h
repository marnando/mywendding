#ifndef LITWIDROW_H
#define LITWIDROW_H

#include <QWidget>

namespace Ui {
    class ListWidRow;
}
class ListWidRow : public QWidget
{
    Q_OBJECT
public:
    explicit ListWidRow(QWidget *parent = 0);
    ~ListWidRow();

    void setWidget(QWidget * a_widget);
    void setRowText(QString a_rowText);

signals:

public slots:

private:
    Ui::ListWidRow * ui;
};

#endif // LITWIDROW_H
