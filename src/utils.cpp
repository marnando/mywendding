#include "utils.h"

#include <QSize>
#include <QApplication>
#include <QDesktopWidget>
#include <QFile>

Utils * Utils::m_utils = new Utils();
Utils::Utils(QObject *parent) : QObject(parent)
{
    m_appIsInit = false;
}

Utils::~Utils()
{
    releaseInstance();
}

Utils *Utils::instace()
{
    if (m_utils == nullptr)
    {
        m_utils = new Utils();
    }

    return m_utils;
}

void Utils::releaseInstance()
{
    if (m_utils != nullptr)
    {
        delete m_utils;
        m_utils = nullptr;
    }
}

void Utils::setInitApp()
{
    m_appIsInit = true;
}

bool Utils::getInitApp()
{
    return m_appIsInit;
}

QSize Utils::getScreenResolution()
{
    return QSize(qApp->desktop()->width(), qApp->desktop()->height());
}

Qt::Orientation Utils::getOrientation()
{
    Qt::Orientation orientation;

    if (qApp->desktop()->width() > qApp->desktop()->height())
    {
        orientation = Qt::Horizontal;
    } else
    {
        orientation = Qt::Vertical;
    }

    return orientation;
}

QString Utils::getStyleFromCss()
{
    QString style;
    QString cssFile;

    #ifdef Q_OS_ANDROID
        cssFile = ":/qss/qss/MyWedding_xxhdpi.css";
    #else
        cssFile = ":/qss/qss/MyWedding_hdpi.css";
    #endif

    QFile styleFile(cssFile);
    if (styleFile.open(QFile::ReadOnly))
    {
        style = QString(styleFile.readAll());
    }

    return style;

}

void Utils::setSizeModalWidget(QWidget *a_widget, int a_parentHeight, int a_parentWidth)
{
    //seto o tamanho da widget em relação do pai
    //defino o valor da proporção em relação do pai
    a_widget->setMinimumHeight(a_parentHeight * 0.80);
    a_widget->setMinimumWidth(a_parentWidth * 0.85);
}

void Utils::setWidgetModal(QWidget *a_miMod, double a_spacer, double a_percentMaxHeigth, double a_percentMaxWidth)
{
    setSizeModalWidget(a_miMod, a_miMod->parentWidget()->height(), a_miMod->parentWidget()->width());

    //Setando haltura e largura geométrica máxima para elementos modais
    if (a_percentMaxHeigth > 0
    &&  a_percentMaxWidth > 0
    &&  a_miMod->parentWidget()) {
        a_miMod->setMaximumHeight(a_miMod->parentWidget()->height() * a_percentMaxHeigth);
        a_miMod->setMaximumWidth(a_miMod->parentWidget()->height() * a_percentMaxWidth);
    }

    //Setando as flags modais e definindo o novo tamanho para widget
    a_miMod->setWindowFlags(Qt::Dialog);
    a_miMod->setWindowModality(Qt::ApplicationModal);
    QDesktopWidget* l_desk = QApplication::desktop();
    QSize l_newsize = QSize (l_desk->width()/a_spacer,l_desk->height()/a_spacer);

    a_miMod->setVisible(true);
    a_miMod->raise();
    a_miMod->resize(l_newsize);
    a_miMod->updateGeometry();

    //Centralizando
    QSize l_size = a_miMod->size();
    int w = l_desk->width();
    int h = l_desk->height();
    int mw = l_size.width();
    int mh = l_size.height();
    int cw = (w/2) - (mw/2);
    int ch = (h/2) - (mh/2);

    a_miMod->move(cw,ch);

    QApplication::setActiveWindow(a_miMod);
    a_miMod->setFocus();
}
