#ifndef UTILS_H
#define UTILS_H

#include <QObject>
#include <QtGlobal>

class Utils : public QObject
{
    Q_OBJECT
private:
    static Utils * m_utils;
    bool m_appIsInit;

public:
    explicit Utils(QObject *parent = 0);
    ~Utils();

    static Utils * instace();
    void releaseInstance();
    void setInitApp();
    bool getInitApp();
    QSize getScreenResolution();
    Qt::Orientation getOrientation();
    QString getStyleFromCss();
    void setSizeModalWidget(QWidget *a_widget, int a_parentHeight, int a_parentWidth);
    void setWidgetModal(QWidget *a_miMod
                      , double a_spacer = 1.5
                      , double a_percentMaxHeigth = 0
                      , double a_percentMaxWidth = 0);
signals:

public slots:
};

#endif // UTILS_H
