#ifndef NCADASTRONOIVOS_H
#define NCADASTRONOIVOS_H

#include <QObject>

class NCadastroNoivos : public QObject
{
    Q_OBJECT
public:
    explicit NCadastroNoivos(QObject *parent = 0);
    ~NCadastroNoivos();

signals:

public slots:
    bool saveGrooms(int a_gender, QString a_noiva, QString a_noivo, quint32 a_dataCasamento, QString a_cidade, QString a_estado);
};

#endif // NCADASTRONOIVOS_H
