#include "error.h"

Error::Error(QObject *parent) : QObject(parent)
{

}

Error::~Error()
{

}

QString Error::getGroomsFormError(quint32 a_codError)
{
    QString text = QString();

    switch (a_codError) {
    case ER_N_SELECT_GENDER:
    {
        text = tr("Necessário selecionar um gênero!");
    }
        break;
    case ER_FIELD_GROOMS:
    {
        text = tr("Informe o nome do casal corretamente!");
    }
        break;
    case ER_WEDDING_DATE:
    {
        text = tr("Informe a data do casamento!");
    }
        break;
    case ER_STATE:
    {
        text = tr("Informe o estado onde ocorrerá o casamento!");
    }
        break;
    case ER_CITY:
    {
        text = tr("Informe a cidade onde ocorrerá o casamento!");
    }
        break;
    default:
        break;
    }
    return text;
}

QString Error::getContactListError(quint32 a_codError)
{
    return QString();
}


