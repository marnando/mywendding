#ifndef ERROR_H
#define ERROR_H

#include <QObject>

class Error : public QObject
{
    Q_OBJECT
public:

    enum cad_noivos {
        ER_N_SELECT_GENDER   = 0x00000001ul,
        ER_FIELD_GROOMS     = 0x00000002ul,
        ER_WEDDING_DATE     = 0x00000004ul,
        ER_STATE            = 0x00000008ul,
        ER_CITY             = 0x00000010ul,
    };

    enum contact_list {

    };

    explicit Error(QObject *parent = 0);
    ~Error();

    static QString getGroomsFormError(quint32 a_codError);
    static QString getContactListError(quint32 a_codError);

signals:

public slots:
};

#endif // ERROR_H
