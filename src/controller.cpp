#include <QSqlDatabase>
#include "controller.h"
#include "utils.h"
#include "control/ccadastronoivos.h"
#include "view/cadastronoivos.h"
#include "view/login.h"
#include "view/logged.h"
#include "view/mainwindow.h"

Controller::Controller(QObject *parent) : QObject(parent)
          , m_mainWindow(nullptr)
          , m_login(nullptr)
          , m_logged(nullptr)
          , m_cadastroNoivos(nullptr)
          , m_db(nullptr)
          , m_cCadNoivos(new CCadastroNoivos(this))
          , m_styleApplication(QString())
{
    init();
}

Controller::~Controller()
{
    if (m_mainWindow != nullptr) { delete m_mainWindow; m_mainWindow = nullptr; }
    if (m_login != nullptr) { delete m_login; m_login = nullptr; }
    if (m_logged != nullptr) { delete m_logged; m_logged = nullptr; }
    if (m_cadastroNoivos != nullptr) { delete m_cadastroNoivos; m_cadastroNoivos = nullptr; }
}

void Controller::init()
{
    //Conecto com o banco de dados
    if (!connection())
    {
//        return;
    }

    //Carrego os estilos
    m_styleApplication = QString(Utils::instace()->getStyleFromCss());

    //Verificar se existe cadastro ja realizado no banco de dados, caso sim, inicializar o Logged
    //Inicio a aplicação, Login
    m_login = new Login();
    m_login->setStyleSheet(m_styleApplication);
    connect(m_login, SIGNAL(initCadastroNoivos()), SLOT(onCadastroNoivos()));

    m_login->show();
}

bool Controller::connection()
{
    bool ok = false;

    //Connectando ao banco
//    m_db = instanceDB();

    if (m_db != nullptr)
    {
        ok = true;
    }

    return ok;
}

void Controller::getConnects()
{
//    qDebug() << " Connects: " <<
//    connect(m_cadastroNoivos, SIGNAL(addNoivos(int,QString,QString,quint32,QString,QString)),
//            m_cCadNoivos,     SLOT(updateGrooms(int,QString,QString,quint32,QString,QString)) );
}

void Controller::onCadastroNoivos()
{
    if (m_cadastroNoivos == nullptr)
    {
        m_cadastroNoivos = new CadastroNoivos();
        m_cadastroNoivos->setStyleSheet(m_styleApplication);
        connect(m_cadastroNoivos, SIGNAL(initApplication()), SLOT(onMainWindow()));
        connect(m_cadastroNoivos, SIGNAL(backToLogin()), SLOT(onLogin()));

        getConnects();
    }

    if (m_login) m_login->hide();
    if (m_logged) m_logged->hide();
    m_cadastroNoivos->show();
}

void Controller::onMainWindow()
{
    if (m_mainWindow == nullptr)
    {
        m_mainWindow = new MainWindow();
        m_mainWindow->setStyleSheet(m_styleApplication);
        Utils::instace()->setInitApp();
    }

    if (m_login != nullptr) { delete m_login; m_login = nullptr; }
    if (m_logged != nullptr) { delete m_logged; m_logged = nullptr; }
    if (m_cadastroNoivos != nullptr) { delete m_cadastroNoivos; m_cadastroNoivos = nullptr; }
    m_mainWindow->show();
}

void Controller::onLogin()
{
    m_login->show();
    m_cadastroNoivos->hide();
}
