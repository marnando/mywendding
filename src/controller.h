#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QObject>
#include "dao/connection.h"

class QSqlDatabase;
class MainWindow;
class Login;
class Logged;
class CadastroNoivos;
class CCadastroNoivos;
class Controller : public QObject
{
    Q_OBJECT
private:
    MainWindow *    m_mainWindow;
    Login *         m_login;
    Logged *        m_logged;
    CadastroNoivos * m_cadastroNoivos;
    QSqlDatabase *  m_db;
    QString         m_styleApplication;

    //Control classes
    CCadastroNoivos * m_cCadNoivos;

public:
    explicit Controller(QObject *parent = 0);
    ~Controller();

private:
    void init(); //Inicia a aplicação
    bool connection(); //Efetua a coneção com o banco de dados
    void getConnects();

signals:

public slots:
    void onCadastroNoivos();
    void onMainWindow();
    void onLogin();
};

#endif // CONTROLLER_H
