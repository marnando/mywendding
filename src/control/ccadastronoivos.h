#ifndef CCADASTRONOIVOS_H
#define CCADASTRONOIVOS_H

#include <QObject>

class NCadastroNoivos;
class CCadastroNoivos : public QObject
{
    Q_OBJECT
private:
    NCadastroNoivos * m_nCadNoivos;

public:
    explicit CCadastroNoivos(QObject *parent = 0);
    ~CCadastroNoivos();

    NCadastroNoivos * nCadNoivos();

signals:
    bool saveGrooms(int a_gender, QString a_noiva, QString a_noivo, quint32 a_dataCasamento, QString a_cidade, QString a_estado);

public slots:
    void updateGrooms(int a_gender, QString a_noiva, QString a_noivo, quint32 a_dataCasamento, QString a_cidade, QString a_estado);
};

#endif // CCADASTRONOIVOS_H
