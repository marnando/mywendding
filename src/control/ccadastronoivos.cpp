#include "ccadastronoivos.h"
#include "../model/ncadastronoivos.h"

CCadastroNoivos::CCadastroNoivos(QObject *parent)
               : QObject(parent)
               , m_nCadNoivos(new NCadastroNoivos(this))
{

}

CCadastroNoivos::~CCadastroNoivos()
{

}

NCadastroNoivos *CCadastroNoivos::nCadNoivos()
{
    return m_nCadNoivos;
}

void CCadastroNoivos::updateGrooms(int a_gender,
                                   QString a_noiva,
                                   QString a_noivo,
                                   quint32 a_dataCasamento,
                                   QString a_cidade,
                                   QString a_estado)
{
    m_nCadNoivos->saveGrooms(a_gender, a_noiva, a_noivo, a_dataCasamento, a_cidade, a_estado);
}

